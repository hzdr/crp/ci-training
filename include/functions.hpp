#pragma once

int add(int const a, int const b) noexcept;
float add(float const a, float const b) noexcept;
double add(double const a, double const b) noexcept;

int mul(int const a, int const b) noexcept;
float mul(float const a, float const b) noexcept;
double mul(double const a, double const b) noexcept;
