# ci-training

Training project to learn how GitLab CI is working.

# requirements

Requirements for the project are:
 - CMake >3.19
 - a C++ 20 supporting compiler 

# build the project

```bash
mkdir build && cd build
cmake ..
cmake --build . -j
# run the application
./app
```

# build and run tests

```bash
mkdir build && cd build
cmake -DBUILD_TESTING=ON ..
cmake --build . -j
# run the application
ctest
```

# Optional features

The functions `featureA()`, `featureB()` and `featureAB()` are not build by default. To enable the functions, add the CMake flag `-DFEATURE_A=ON` and/or `-DFEATURE_B=ON`. If both flags are enable, the function `featureAB()` will be enabled.

```bash
mkdir build && cd build
# enable featureA()
cmake .. -DFEATURE_A=ON
cmake --build . -j
# run the application
./app
```

```bash
mkdir build && cd build
# enable featureB()
cmake .. -DFEATURE_B=ON
cmake --build . -j
# run the application
./app
```

```bash
mkdir build && cd build
# enable featureA(), featureB() and featureAB()
cmake .. -DFEATURE_A=ON -DFEATURE_B=ON
cmake --build . -j
# run the application
./app
```
