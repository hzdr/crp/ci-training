#include "featureA.hpp"
#include "featureB.hpp"

int featureAB(int ab) { return featureA(ab) + featureB(ab); }
