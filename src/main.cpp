#include "featureA.hpp"
#include "featureAB.hpp"
#include "featureB.hpp"
#include "functions.hpp"

#include <iostream>

int main(int argc, char **argv) {
  std::cout << "add(1, 3): " << add(1, 3) << "\n";
  std::cout << "add(3.1, 1.1): " << add(3.1, 1.1) << "\n";

  std::cout << "\n";
  std::cout << "mul(3, 4): " << mul(3, 4) << "\n";
  std::cout << "mul(2.f, 1.4f): " << mul(2.f, 1.4f) << "\n";

#ifdef FEATURE_A
  std::cout << "\n";
  std::cout << "featureA(3): " << featureA(3) << "\n";
#endif

#ifdef FEATURE_B
  std::cout << "\n";
  std::cout << "featureB(3): " << featureB(3) << "\n";
#endif

#if defined(FEATURE_A) && defined(FEATURE_B)
  std::cout << "\n";
  std::cout << "featureAB(3): " << featureAB(3) << "\n";
#endif

  return 0;
}
