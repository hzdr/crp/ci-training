#include "functions.hpp"

int add(int const a, int const b) noexcept { return a + b; }
float add(float const a, float const b) noexcept { return a + b; }
double add(double const a, double const b) noexcept { return a + b; }

int mul(int const a, int const b) noexcept { return a * b; }
float mul(float const a, float const b) noexcept { return a * b; }
double mul(double const a, double const b) noexcept { return a * b; }
