#include <catch2/catch_test_macros.hpp>

#include "functions.hpp"

template <typename T> bool approx_equal(T const a, T const b, T const sigma) {
  return std::abs(a - b) < sigma;
}

TEST_CASE("test mul with int", "[mul]") { REQUIRE(mul(3, 7) == 21); }

TEST_CASE("test mul with float", "[mul]") {
  REQUIRE(approx_equal(mul(3.4f, 2.7f), 9.18f, 0.00001f));
}

TEST_CASE("test mul with double", "[mul]") {
  REQUIRE(approx_equal(mul(7.89, 4.23), 33.3747, 0.00001));
}
