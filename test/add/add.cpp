#include <catch2/catch_test_macros.hpp>

#include "functions.hpp"

template <typename T> bool approx_equal(T const a, T const b, T const sigma) {
  return std::abs(a - b) < sigma;
}

TEST_CASE("test add with int", "[add]") { REQUIRE(add(1, 5) == 6); }

TEST_CASE("test add with float", "[add]") {
  REQUIRE(approx_equal(add(3.4f, 2.7f), 6.1f, 0.00001f));
}

TEST_CASE("test add with double", "[add]") {
  REQUIRE(approx_equal(add(7.89, 4.23), 12.12, 0.00001));
}
