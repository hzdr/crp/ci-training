import yaml

def gen_job(job_name: str, extra_cmake_args: str, jobs):
    job = {}
    job["stage"] = "test"
    job["image"] = "ubuntu:22.04"
    job["script"] = [
        'apt-get update',
        "apt-get install -y cmake g++ git",
        "mkdir build",
        "cd build",
       f"cmake -DBUILD_TESTING=ON {extra_cmake_args} ..",
        "cmake --build . -j",
        "ctest",
        "./app"
    ]
    jobs[job_name] = job

if __name__ == "__main__":
    jobs = {}
    jobs["stages"] = ["test"]

    job_definitions = [
        ("build", ""), 
        ("Atest", "-DFEATURE_A=ON"), 
        ("Btest", "-DFEATURE_B=ON"), 
        ("ABtest", "-DFEATURE_A=ON -DFEATURE_B=ON")
    ]
    
    for name, cmake_arg in job_definitions:
        gen_job(name, cmake_arg, jobs)

    with open('generator.gitlab-ci.yml', 'w') as file:
        yaml.dump(jobs, file, default_flow_style=False)
